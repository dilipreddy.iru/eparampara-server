const express = require('express');
const router = express.Router(); 
const mongoose = require('mongoose')

const Employee = require('../models/employee')


router.get('/', (req, res, next)=>{
    Employee.find()
   .exec()
   .then(docs =>{
       console.log(docs);
       res.status(200).json(docs)
       
   })
   .catch(err =>{
       console.log(err);
       res.status(500).json({error: err})
       
   })
} )

router.post('/', (req, res, next)=>{
    const product= new Employee({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        code: req.body.code,
        address: req.body.address
        // price: req.body.price,
    });
    product.save().then(result =>{
        console.log(result);
        res.status(201).json({
            message: 'handling  post req to products',
            isValid: true,
            createdProduct: result
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err})
    });
   
} )

router.get('/:productId', (req, res, next)=>{
   const id = req.params.productId;
   Employee.findById(id)
   .exec()
   .then(doc => {
       console.log(doc);
       if(doc){
        res.status(200).json(doc)
       }else{
           res.status(404).json({message: 'No valid entry found'})
       }
   })
   .catch(err => {
    console.log(err)
    res.status(500).json({error: err})
   })
})

router.patch('/:productId', (req, res, next)=>{
    const id = req.params.productId;
    // const updateOps = {}
    // for (const ops of req.body){
    //     updateOps[ops.propName] = ops.value;
    // }
    
    Employee.update({_id : id}, {$set: {name: req.body.name, code: req.body.code, address: req.body.address}})
     .exec()
     .then(result =>{
         console.log(result);
         res.status(200).json(result)
     })
     .catch(err =>{
         console.log(err);
         res.status(500).json({error: err})
         
     })

 })

 router.delete('/:productId', (req, res, next)=>{
   const id = req.params.productId
   Employee.remove({_id: id})
   .exec()
   .then(result => {
    res.status(200).json(result);
       
   })
   .catch(err => {
       console.log(err);
       res.status(500).json({error: err})
       
   })
})
module.exports = router;