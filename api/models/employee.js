const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    code: Number,
    address: String
})

module.exports = mongoose.model('Employee', employeeSchema)