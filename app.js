const express =  require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')


const employeeRoutes = require('./api/routes/employees');
const userRoutes = require('./api/routes/user');
mongoose.connect('mongodb+srv://dilip:toDo@todo-app.awmqp.mongodb.net/todo?retryWrites=true&w=majority', {
    useUnifiedTopology: true,
    useNewUrlParser: true
})

app.use(morgan('dev'));
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({extended: false}));

// app.use((req, res, next) =>{
//     res.header('Access-Control-Origin', '*');
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
//     // if(req.method === 'OPTIONS'){
//     //  res.header("Access-Control-Allow-Headers", "PUT, POST, PATCH, DELETE, GET")   
//     //  return res.status(200).json({})
//     // }
// })

app.use('/employees', employeeRoutes);
app.use("/user", userRoutes);

app.use((req, res, next) =>{
    const error = new Error('Not Found');
    error.status = 404;

    next(error)
})


app.use((error, req, res, next) =>{
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
})
module.exports = app;